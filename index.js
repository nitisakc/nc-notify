'use strict'
const axios = require('axios');
const querystring = require('querystring');
const mailer = require("nodemailer");

class Line{
    constructor(token) {
        this.token = token;
    }
    send(msg){
        if(Array.isArray(msg)){ msg = msg.join('\n'); }
        return new Promise((resolve, reject)=>{
            axios({
                method: 'post',
                url: 'https://notify-api.line.me/api/notify',
                headers: {
                  'Authorization': 'Bearer ' + this.token,
                  'Content-Type': 'application/x-www-form-urlencoded',
                  'Access-Control-Allow-Origin': '*'
                },
                data: querystring.stringify({
                  message: msg,
                })
              })
            .then( function(res) {
                resolve(res.data);
            })
            .catch( function(err) {
                reject(err);
            });
        });
    }
}

class MailSmtp{
    constructor(options) {
        this.options = options;
    }
    send(options){
        // {
        //     host: 'host.email.com', 
        //     port: 25, 
        //     secure: true, 
        //     auth: {
        //       user: 'user@email.com', 
        //       pass: 'password' 
        //     }
        //   };
        const smtpTransport = mailer.createTransport(this.options);
        let mail = {
            from: 'from@email.com', 
            to: 'to@email.com', 
            subject: "Subject Text", 
            html: `<p>Test</p>`,  
            attachments: [{ path : '' }]
         }
        smtpTransport.sendMail(mail, function(error, response){
            smtpTransport.close();
            if(error){
               //error handler
            }else{
               //success handler 
            }
         });
    }
}

class Slack{
    constructor(token) {
        this.token = token;
    }
    send(msg){
    }
}

class Discord{
    constructor(token) {
        this.token = token;
    }
    send(msg){
    }
}

class Telegram{
    constructor(token) {
        this.token = token;
    }
    send(msg){
    }
}

module.exports = { Line, MailSmtp, Slack, Discord, Telegram };